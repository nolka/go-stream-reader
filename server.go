package main

import (
	"github.com/gin-gonic/gin"
)

type WebServer struct {
	Engine        *gin.Engine
	ListenAddress string
	Commands      chan Command
	Results       chan Result
	StationManager *StationManager
}

func CreateServer(address string, commands chan Command, results chan Result) *WebServer {
	s := &WebServer{
		Engine:        gin.Default(),
		ListenAddress: address,
		Commands:      commands,
		Results:       results,
	}
	return s
}

func (s *WebServer) Start() {
	s.AttachHandlers()
	s.Engine.Run(s.ListenAddress)
}

func (s *WebServer) AttachHandlers() {
	s.Engine.GET("/ping", func(c *gin.Context) {
		c.String(204, "")
	})

	s.Engine.GET("/stat", func(c *gin.Context) {
		s.Commands <- &Stat{}

		result := <-s.Results
		c.JSON(200, result.(StatResult))
	})

}
