package util

import (
	"crypto/tls"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"reflect"

	"github.com/PuerkitoBio/goquery"
)

func GetQuery(url string) *goquery.Document {
	client := &http.Client{
	}
	mainPage, err := client.Get(url)
	if err != nil {
		log.Printf("Failed to load main page: %s\n", err)
		return nil
	}
	defer mainPage.Body.Close()

	if mainPage.StatusCode != 200 {
		log.Printf("Failed to precess main page because StatusCode  is %d, must be 200", mainPage.StatusCode)
		return nil
	}

	parser, err := goquery.NewDocumentFromReader(mainPage.Body)
	if err != nil {
		log.Printf("Failed to create parser: %s", err)
		return nil
	}

	return parser
}

func FileExist(filename string) bool {
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		return false
	}
	return true
}

func IndexOf(v interface{}, in interface{}) (ok bool, i int) {
	val := reflect.Indirect(reflect.ValueOf(in))
	switch val.Kind() {
	case reflect.Slice, reflect.Array:
		for ; i < val.Len(); i++ {
			if ok = v == val.Index(i).Interface(); ok {
				return
			}
		}
	}
	return
}

func InArray(v interface{}, in interface{})  bool {
	exist, _ := IndexOf(v, in)
	return  exist
}

func GetFileAsString(url string, client *http.Client) (content string, err error) {
	if client == nil {
		client = &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
			},
		}
	}
	resp, err := client.Get(url)

	if err != nil {
		log.Printf("Failed to get url: %s", err)
		return "", err
	}
	defer resp.Body.Close()

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("Failed to read response body: %s", err)
		return "", err
	}
	return string(bodyBytes), nil
}