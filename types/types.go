package types

type StreamData struct {
	MediaType string `json:"mediaType"`
	Mime      string `json:"mime"`
	Url       string `json:"url"`
}

type StationInfo struct {
	Name     string `json:"name"`
	Title    string `json:"title"`
	Url      string `json:"url"`
	UsePopup bool   `json:"usePopup"`
}

type StationData struct {
	Info    StationInfo  `json:"station"`
	Streams []StreamData `json:"streams"`
}

type Stats struct {
	BytesProcessed uint64 `json:"bytesProcessed"`
	ErrorsCount    uint `json:"errorsCount"`
}
