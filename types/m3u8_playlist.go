package types

import (
	"errors"
	"fmt"

	"bitbucket.org/nolka/go-stream-reader/util"
)

type PlayListM3U8 struct {
	files   []string
	current int
}

func (pl *PlayListM3U8) Append(url string) {
	if !util.InArray(url, pl.files) {
		pl.files = append(pl.files, url)
	}
}

func (pl *PlayListM3U8) GetNext() (string, error) {
	var url string
	if pl.current < pl.GetCount() {
		url = pl.files[pl.GetCurrentId()]
		pl.current++
	} else {
		return "", errors.New("Playlist was empty")
	}
	return url, nil
}

func (pl *PlayListM3U8) GetCurrent() string {
	return pl.files[pl.GetCurrentId()]
}

func (pl *PlayListM3U8) GetAll() []string {
	return pl.files
}

func (pl *PlayListM3U8) GetCount() int {
	return len(pl.files)
}

func (pl *PlayListM3U8) GetCurrentId() int {
	return pl.current
}

func (pl *PlayListM3U8) IsEndReached() bool {
	return pl.GetCurrentId() >= pl.GetCount()
}

func (pl *PlayListM3U8) Load(files []string) {
	pl.files = files
	pl.current = 0
}

func (pl *PlayListM3U8) Shift(count int) error {
	if count > pl.GetCount() {
		return errors.New(fmt.Sprintf("Cannot shift %d elements because total count of items is %d", count, pl.GetCount()))
	}

	pl.files = pl.files[count:]
	pl.current -= count - 1
	return nil
}
