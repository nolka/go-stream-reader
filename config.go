package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"

	"bitbucket.org/nolka/go-stream-reader/util"
)

type Config struct {
	StoragePath string
}

func getAppDir() string {
	path, err := os.Executable()
	if err != nil {
		log.Fatalf("Failed to get app dir")
	}
	return filepath.Dir(path)
}

func getConfig() *Config {
	configFileName := getAppDir() + string(os.PathSeparator) + "config.json"
	cfg := &Config{}
	if !util.FileExist(configFileName) {
		log.Printf("Config file was not exist")
		cfg.StoragePath = getAppDir() + string(os.PathSeparator) + "streams"
		return cfg
	}
	fh, err := os.Open(configFileName)
	cfgbytes, err := ioutil.ReadAll(fh)

	err = json.Unmarshal(cfgbytes, cfg)
	if err != nil {
		log.Printf("Failed to parse Config: %s", err)
		return nil
	}
	return cfg
}
