package interfaces

import "bitbucket.org/nolka/go-stream-reader/types"

type StreamReaderAdapterInterface interface {
	Init()
	Read([]byte) (int, error)
	IsReading() bool
	GetStreamSelected() *types.StreamData
}
