package main

import (
	"log"
)

func startRearers() {
	log.Println("Starting StationManager...")
	config := getConfig()
	commands := make(chan Command)
	results := make(chan Result)

	sm := CreateStationManager(commands, results)
	go sm.ListenStreams(config.StoragePath)

	log.Println("Starting WebServer...")
	s := CreateServer(":8081", commands, results)
	s.StationManager = sm
	s.Start()
}

func main() {
	startRearers()

	//sm := CreateStationManager(nil, nil)
	//m3u := sm.GetStationData("https://api.webrad.io/data/streams/74/radio-7-104-7-fm-moskva")
	//reader := CreateReader(m3u, "/tmp/streams")
	//reader.ReadStream(m3u)

}
