package streams

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"bitbucket.org/nolka/go-stream-reader/interfaces"
	"bitbucket.org/nolka/go-stream-reader/types"
)

const (
	READ_BUFF_SIZE = 4096
)

type StreamReader struct {
	StationData   types.StationData `json:"stationData"`
	ReaderAdapter interfaces.StreamReaderAdapterInterface
	Stats         *types.Stats
	StoragePath   string
	hour          string
}

func (s *StreamReader) ReadStream(stationData *types.StationData) {
	if s.ReaderAdapter == nil {
		log.Printf("Stream reader was set found for station: %s", s.StationData.Info.Name)
	}

	s.ReaderAdapter.Init()
	s.SetHour()

	dst := s.GetDestFile(s.ReaderAdapter.GetStreamSelected().MediaType)
	if dst == nil {
		return
	}

	chunk := make([]byte, READ_BUFF_SIZE)
	var readCount = 0
	var writtenCount = 0
	var err error
	for {
		readCount, err = s.ReaderAdapter.Read(chunk)
		if err == io.EOF {
			log.Printf("End of stream detected for: %s", stationData.Info.Name)
			break
		}

		if err != nil {
			log.Printf("Error reading stream: %s", err)
			break
		}

		if s.hour != s.GetHour() {
			dst.Close()
			s.hour = s.GetHour()
			dst = s.GetDestFile(s.ReaderAdapter.GetStreamSelected().MediaType)
		}
		writtenCount, err = dst.Write(chunk[:readCount])
		if err != nil {
			log.Printf("Failed to write stream to disk: %s", err)
		}
		s.Stats.BytesProcessed += uint64(writtenCount)
	}
	dst.Close()
}

func (s *StreamReader) IsReading() bool {
	if s.ReaderAdapter == nil {
		return false
	}
	return s.ReaderAdapter.IsReading()
}

func (s *StreamReader) IsCanRead() bool {
	return s.ReaderAdapter != nil
}

func (s *StreamReader) GetStationData() *types.StationData {
	return &s.StationData
}

func (s *StreamReader) GetStats() *types.Stats {
	return s.Stats
}

func (s *StreamReader) GetDestFile(ext string) *os.File {
	path := s.GetFilePath()
	s.VerifyPath(path)
	fn := path + string(os.PathSeparator) + s.GetFileName() + "." + ext
	dst, err := os.OpenFile(fn, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
	if err != nil {
		log.Printf("Cannnot append file: %s: %s", fn, err.Error())
		s.Stats.ErrorsCount++
		return nil
	}
	return dst
}

func (s *StreamReader) GetFilePath() string {
	t := time.Now()
	pathChunks := []string{s.StoragePath, s.StationData.Info.Name, fmt.Sprintf("%d-%02d-%02d", t.Year(), t.Month(), t.Day())}

	return strings.Join(pathChunks, string(os.PathSeparator))
}

func (s *StreamReader) VerifyPath(path string) {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		err := os.MkdirAll(path, os.ModePerm)
		if err != nil {
			log.Printf("Failed to create path: %s: %s", path, err.Error())
			s.Stats.ErrorsCount++
		}
	}
}

func (s *StreamReader) GetFileName() string {
	path := []string{s.hour}
	return strings.Join(path, string(os.PathSeparator))
}

func (s *StreamReader) GetHour() string {
	return fmt.Sprintf("%02d", time.Now().Hour())
}

func (s *StreamReader) SetHour() {
	s.hour = s.GetHour()
}

func (s *StreamReader) GetExtension(headers *http.Header) string {
	return headers.Get("Content-Type")[strings.Index(headers.Get("Content-Type"), "/")+1:]
}
