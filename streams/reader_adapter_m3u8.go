package streams

import (
	"crypto/tls"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/nolka/go-stream-reader/types"
	"bitbucket.org/nolka/go-stream-reader/util"
)

const (
	PLAYLIST_SHIFT_COUNT = 10
	PLAYLIST_MIN_FILES_COUNT = 10
	PLAYLIST_MAX_FILES_COUNT = 10
)

type BaseStreamReaderAdapter struct {
	StationData    *types.StationData
	StreamSelected *types.StreamData
	Stats          *types.Stats
	Client         *http.Client `json:"-"`
	isReading      bool
}

func (b *BaseStreamReaderAdapter) IsReading() bool {
	return b.isReading
}

func (b *BaseStreamReaderAdapter) IsCanRead() bool {
	return b.StreamSelected != nil
}

func (b *BaseStreamReaderAdapter) GetHttpClient() *http.Client {
	return &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		},
	}
}

type PlayListStreamReader struct {
	*BaseStreamReaderAdapter
	Response     *http.Response `json:"-"`
	playlistUrl  string
	playlist     *types.PlayListM3U8
	openedStream *io.Reader `json:"-"`
	lastUpdateTs int64
}

func (p *PlayListStreamReader) GetStreamSelected() *types.StreamData {
	return p.StreamSelected
}

func (p *PlayListStreamReader) Init() {
	p.Client = p.GetHttpClient()

	p.isReading = true

	p.playlistUrl = p.GetPlaylistUrl(p.StreamSelected.Url)
}

func (p *PlayListStreamReader) GetPlaylistUrl(url string) string {
	selectedPls := ""

	content, err := util.GetFileAsString(url, p.Client)
	if err != nil {
		return ""
	}
	lines := strings.Split(content, "\n")

	for _, line := range lines {
		if !strings.HasPrefix(line, "#") {
			selectedPls = line
			break
		}
	}
	selectedPls = url[:strings.LastIndex(url, "/")+1] + selectedPls
	return selectedPls
}

func (p *PlayListStreamReader) GetPlaylistFiles(url string) []string {
	files := []string{}

	content, err := util.GetFileAsString(url, p.Client)
	if err != nil {
		return files
	}
	lines := strings.Split(content, "\n")

	for _, line := range lines {
		if !strings.HasPrefix(line, "#") && len(line) > 0 {
			files = append(files, url[:strings.LastIndex(url, "/")+1]+line)
		}
	}
	return files
}

func (p *PlayListStreamReader) UpdateStreamedFiles() bool {
	for _, fileUrl := range p.GetPlaylistFiles(p.playlistUrl) {
		p.playlist.Append(fileUrl)
	}

	if p.playlist.GetCount() > PLAYLIST_MAX_FILES_COUNT && (p.playlist.GetCount() - PLAYLIST_SHIFT_COUNT) > PLAYLIST_MIN_FILES_COUNT {
		p.playlist.Shift(PLAYLIST_SHIFT_COUNT)
	}

	return p.playlist.IsEndReached()
}

func (p *PlayListStreamReader) Read(buf []byte) (int, error) {
	for p.playlist.IsEndReached() || p.playlist.GetCount() == 0 {
		p.UpdateStreamedFiles()
		time.Sleep(time.Millisecond * 1200)
	}

	var err error
	var readCount int
	if p.Response == nil {
		var err error
		fileUrl, err := p.playlist.GetNext()
		if err != nil {
			p.Stats.ErrorsCount++
			p.isReading = false
			log.Printf("Failed to get next file url: %s", err)
			return 0, err
		}

		p.Response, err = p.Client.Get(fileUrl)
		if err != nil {
			log.Printf("Failed to get file from playlist: %s %s", fileUrl, err)
		}
		if p.Response.StatusCode != 200 {
			p.isReading = false
			return 0, errors.New(fmt.Sprintf("Status code returned: %d", p.Response.StatusCode))
		}
	}
	p.isReading = true
	readCount, err = p.Response.Body.Read(buf)

	if err == io.EOF {
		p.Response.Body.Close()
		p.Response = nil
		p.Stats.BytesProcessed += uint64(readCount)
		p.isReading = false
		err = nil
	}

	if err != nil {
		log.Printf("Error reading file: %s %s", p.playlist.GetCurrent(), err)
		p.Response = nil
		p.Stats.ErrorsCount++
		p.isReading = false
	}

	p.Stats.BytesProcessed += uint64(readCount)
	return readCount, err
}
