package streams

import (
	"log"

	"bitbucket.org/nolka/go-stream-reader/types"
	"bitbucket.org/nolka/go-stream-reader/util"
)

type LoveRadioStreamReader struct {
	*StreamReader
}

func (s *LoveRadioStreamReader) GetStreamData() *types.StreamData {
	doc := util.GetQuery("http://www.loveradio.ru/player.htm")
	if doc == nil {
		log.Printf("Failed to get LoveRadio stream page!")
		return nil
	}
	url, exist := doc.Find("audio").First().Attr("src")
	if !exist {
		log.Printf("Failed to get stream URL because it does not exist")
		return nil
	}
	sd := &types.StreamData{
		Url:       url,
		Mime:      "audio/aac",
		MediaType: "AAC",
	}
	return sd
}
