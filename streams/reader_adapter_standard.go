package streams

import (
	"crypto/tls"
	"io"
	"log"
	"net/http"

	"bitbucket.org/nolka/go-stream-reader/types"
)

type StandardStreamReader struct {
	*BaseStreamReaderAdapter
	Response *http.Response `json:"-"`
}

func (p *StandardStreamReader) GetStreamSelected() *types.StreamData {
	return p.StreamSelected
}

func (p *StandardStreamReader) Init() {
	p.Client = &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		},
	}
	var err error
	p.Response, err = p.Client.Get(p.StreamSelected.Url)
	if err != nil {
		log.Printf("Failed to read stream: %s %s: %s", p.StationData.Info.Name, p.StreamSelected.Url, err)
		p.Response = nil
		p.Stats.ErrorsCount++
		p.isReading = false
		return
	}
	p.isReading = true
}

func (p *StandardStreamReader) Read(buf []byte) (int, error) {
	if p.Response == nil {
		return -1, io.EOF
	}
	readCount, err := p.Response.Body.Read(buf)
	if err != nil {
		p.Stats.ErrorsCount++
		p.isReading = false
		return -1, io.EOF
	}
	p.Stats.BytesProcessed += uint64(readCount)
	return readCount, err
}
