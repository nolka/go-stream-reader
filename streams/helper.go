package streams

import (
	"strings"
	"time"

	"bitbucket.org/nolka/go-stream-reader/interfaces"
	"bitbucket.org/nolka/go-stream-reader/types"
	"bitbucket.org/nolka/go-stream-reader/util"
)

func GetStreamReaderAdapter(data *types.StationData) interfaces.StreamReaderAdapterInterface {
	var sr interfaces.StreamReaderAdapterInterface
	streamSelected := SelectStream(data)
	if streamSelected == nil {
		return nil
	}
	if strings.HasSuffix(streamSelected.Url, "m3u8") {
		sr = &PlayListStreamReader{
			&BaseStreamReaderAdapter{
				StationData:    data,
				StreamSelected: streamSelected,
				Stats:          &types.Stats{},
				Client:         nil,
				isReading:      false,
			},
			nil,
			"",
			&types.PlayListM3U8{},
			nil,
			time.Now().Unix(),
		}
	}
	if util.InArray(strings.ToLower(streamSelected.MediaType), []string{"mp3", "aac"}) {
		sr = &StandardStreamReader{
			&BaseStreamReaderAdapter{
				StationData:    data,
				StreamSelected: streamSelected,
				Stats:          &types.Stats{},
				Client:         nil,
				isReading:      false,
			},
			nil,
		}
	}
	if sr == nil {
		return nil
	}
	return sr
}

func SelectStream(data *types.StationData) *types.StreamData {
	for _, data := range data.Streams {
		dataMediaType := strings.ToLower(data.MediaType)
		if util.InArray(dataMediaType, []string{"mp3", "aac", "hls"}) && !strings.HasSuffix(data.Url, ".m3u") {
			return &data
		}
	}
	return nil
}
