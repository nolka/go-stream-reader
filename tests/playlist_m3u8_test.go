package tests

import (
	"testing"

	"bitbucket.org/nolka/go-stream-reader/types"
)

func TestPlayListAppend(t *testing.T) {
	p := &types.PlayListM3U8{}

	p.Append("file1")
	if p.GetCurrentId() != 0 {
		t.Error("Current id must be 0")
	}

	if f, _ := p.GetNext(); f != "file1" {
		t.Error("GetNext() must return file1 string")
	}

	if p.GetCurrentId() != 1 {
		t.Error("Current id must be equal to 1")
	}

	if _, err := p.GetNext(); err == nil {
		t.Error("GetNext() must return error because list of files is empty")
	}

	p.Append("file1")
	p.Append("file1")
	if p.GetCount() != 1 {
		t.Error("Playlist must have no doubles")
	}

	p.Append("file2")
	if p.GetCount() != 2 {
		t.Error("Playlist size must equal to 2")
	}

	if p.Shift(2) != nil {
		t.Error("Playlist must be shifted to 0 element")
	}

	if p.GetCount() != 0 {
		t.Errorf("Playlist must have zero elements, got %d items", p.GetCount())
	}

	if p.GetCurrentId() != 0 {
		t.Errorf("Playlist current index must point to zero element, but points to %d", p.GetCurrentId())
	}
}
