package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/nolka/go-stream-reader/streams"
	"bitbucket.org/nolka/go-stream-reader/types"
	"bitbucket.org/nolka/go-stream-reader/util"
	"github.com/PuerkitoBio/goquery"
)

type StationManager struct {
	Commands chan Command
	Results  chan Result
	Readers  []*streams.StreamReader
}

func CreateStationManager(commands chan Command, results chan Result) *StationManager {
	sm := &StationManager{
		Commands: commands,
		Results:  results,
	}
	return sm
}

func CreateReader(stationData *types.StationData, storagePath string) *streams.StreamReader {
	log.Printf("Create station logger for: %s", stationData.Info.Name)
	return &streams.StreamReader{
		StationData:   *stationData,
		StoragePath:   storagePath,
		Stats:         &types.Stats{},
		ReaderAdapter: streams.GetStreamReaderAdapter(stationData),
	}
}

func (s *StationManager) ListenStreams(storageDir string) {

	log.Println("Getting stations...")
	for _, stationData := range s.GetStreams() {
		s.Readers = append(s.Readers, CreateReader(stationData, storageDir))
	}
	go s.HandleCommands()
	for {
		for _, streamLog := range s.Readers {
			if streamLog.IsCanRead() && !streamLog.IsReading() {
				go streamLog.ReadStream(streamLog.GetStationData())
			}
		}
		time.Sleep(5 * time.Second)
	}
}

func (s *StationManager) HandleCommands() {
	for cmd := range s.Commands {
		cmd.Execute(s, s.Results)
	}
}

func (s *StationManager) GetStreams() []*types.StationData {
	baseUrl := "https://api.webrad.io/data/streams/74"
	stationDataList := []*types.StationData{}

	doc := util.GetQuery("https://radio.pp.ru/")
	if doc == nil {
		return stationDataList
	}
	doc.Find("ul#radios a").Each(func(i int, sel *goquery.Selection) {
		href, exist := sel.Attr("href")
		if !exist {
			return
		}

		stationUrl := fmt.Sprintf("%s/%s", baseUrl, href[strings.Index(href, "#")+1:])
		station := s.GetStationData(stationUrl)
		stationDataList = append(stationDataList, station)
	})
	return stationDataList
}

func (s *StationManager) GetStationData(url string) *types.StationData {
	resp, err := http.Get(url)
	if err != nil {
		log.Printf("Failed to get StationData: %s", url)
		return nil
	}
	defer resp.Body.Close()

	cfgbytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("Failed to read response body")
	}

	stationData := &types.StationData{}
	err = json.Unmarshal(cfgbytes, stationData)
	if err != nil {
		log.Printf("Failed to parse StationData: %s", err)
		return nil
	}
	return stationData
}
