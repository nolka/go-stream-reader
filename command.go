package main

import (
	"runtime"
)

type Command interface {
	Execute(sm *StationManager, results chan Result)
}

type Result interface {
}

type Stat struct {
}

type StatResult struct {
	IsSuccess bool
	Memstat interface{}
	Readers interface{}
}

func (s *Stat) Execute(sm *StationManager, results chan Result) {
	var m runtime.MemStats
	runtime.ReadMemStats(&m)

	memstat := struct{
		MemUsage uint64
		Uptime uint
		GcCount uint32
		TotalBytesRead uint64
	}{
		m.Alloc,
		0,
		m.NumGC,
		0,
	}
	for _, sr := range sm.Readers {
		memstat.TotalBytesRead += sr.Stats.BytesProcessed
	}

	res := StatResult{}

	res.IsSuccess = true
	res.Memstat = memstat
	res.Readers = sm.Readers

	results <- res
}
